public class Volunteer extends StaffMember {
    public Volunteer(int id,String name,String address){
        super(id, name, address);
    }

    @Override
    public String toString() {
        return super.toString()+"\nThank you!";
    }

    @Override
    double pay() {
        return 0;
    }
}
