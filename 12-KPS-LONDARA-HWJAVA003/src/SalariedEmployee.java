public class SalariedEmployee extends StaffMember {
    private double salary;
    private double bonus;
    public SalariedEmployee(int id,String name,String address,double salary,double bonus){
        super(id, name, address);
        this.salary = salary;
        this.bonus = bonus;
    }

    @Override
    public String toString() {
        return super.toString()+"\nSalary = "+salary+"\nBonus = "+bonus+"\nPayment = "+pay();
    }
    @Override
    double pay() {
        if(bonus>0 && salary>0) {
            return (bonus + salary);
        }else {
            return 0;
        }
    }
    public void setSalary(double salary){
        this.salary = salary;
    }
    public void setBonus(double bonus){
        this.bonus = bonus;
    }
}
