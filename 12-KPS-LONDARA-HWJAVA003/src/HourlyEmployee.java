public class HourlyEmployee extends StaffMember {
    private int hourWorked;
    private double rate;
    public HourlyEmployee(int id,String name,String address,int hw,double rate){
        super(id, name, address);
        this.hourWorked = hw;
        this.rate = rate;
    }
    @Override
    public String toString() {
        return (super.toString()+"\nHour = "+hourWorked+"\nRate = "+rate+"\nPayment = "+pay());
    }
    @Override
    double pay() {
        double hour = Double.valueOf(hourWorked);
        if(hour > 0 && rate>0)
            return hour*rate;
        else
            return 0;
    }

    public void setHourWorked(int hourWorked) {
        this.hourWorked = hourWorked;
    }

    public void setRate(double rate) {
        this.rate = rate;
    }

    public int getHourWorked() {
        return hourWorked;
    }

    public double getRate() {
        return rate;
    }
}
