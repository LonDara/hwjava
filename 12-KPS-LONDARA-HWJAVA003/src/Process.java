import com.sun.imageio.plugins.wbmp.WBMPImageReader;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Scanner;

public class Process {
    ArrayList staff = new ArrayList();
    StaffMember st;
    Scanner sc = new Scanner(System.in);
    public void SetList(){
        StaffMember st0 = new Volunteer(1,"lon dara","phome penh");
        StaffMember st1 = new SalariedEmployee(2,"kim long","phome penh",300,50);
        StaffMember st2 = new HourlyEmployee(3,"Ton chanda","phome penh",12,20);
        staff.add(st0);
        staff.add(st1);
        staff.add(st2);
    }
    public void Dispaly(){
        Collections.sort(staff, StaffMember.StuNameComparator);
        System.out.println("========================================================");
        for(int i=0;i<staff.size();i++){
            System.out.println(staff.get(i).toString());
            System.out.println("========================================================\n");
        }
//        System.out.println(staff.size());
    }
    public void Add(){
        boolean Run = true;
        while (Run==true){
            System.out.print("1. Volunteer Employee   ");
            System.out.print("2. Salaried Employee    ");
            System.out.print("3. Hourly Employee      ");
            System.out.println("4. Back \n");
            System.out.print("Choose option(1->4) : ");
            while(!sc.hasNextInt()){
                String input = sc.next();
                System.out.print("Choose option(1->4) :");
            }
            int chs = sc.nextInt();
            switch (chs) {
                case 1:
                    System.out.println("===================== INSERT INFO ======================");
                    String Vname;
                    System.out.print("=>Enter Staff's ID : ");
                    while(!sc.hasNextInt()){
                        System.out.print("=>Enter Staff's ID : ");
                        String input = sc.next();
                    }
                    int Vid = sc.nextInt();
                    System.out.print("=>Enter Staff's Name : ");
                    sc.nextLine();
                    Vname = sc.nextLine();
                    System.out.print("=>Enter Staff's Address : ");
                    String Vaddress = sc.nextLine();
                    System.out.println("========================================================");
                    st = new Volunteer(Vid, Vname, Vaddress);
                    staff.add(st);
                    break;
                case 2:
                    System.out.println("===================== INSERT INFO ======================");
                    System.out.print("=>Enter Staff's ID : ");
                    while(!sc.hasNextInt()){
                        String input = sc.next();
                        System.out.print("=>Enter Staff's ID : ");
                    }
                    int Sid = sc.nextInt();
                    System.out.print("=>Enter Staff's Name : ");
                    sc.nextLine();
                    String Sname = sc.nextLine();
                    System.out.print("=>Enter Staff's Address : ");
                    String Saddress = sc.nextLine();
                    System.out.print("=>Enter Staff's Salary : ");
                    while(!sc.hasNextDouble()){
                        String input = sc.next();
                        System.out.print("=>Enter Staff's Salary : ");
                    }
                    double Ssalary = sc.nextDouble();
                    System.out.print("=>Enter Staff's Bonus : ");
                    while(!sc.hasNextDouble()){
                        String input = sc.next();
                        System.out.print("=>Enter Staff's Bonus : ");
                    }
                    double Sbouns = sc.nextDouble();
                    System.out.println("========================================================");
                    st = new SalariedEmployee(Sid, Sname, Saddress, Ssalary, Sbouns);
                    staff.add(st);
                    break;
                case 3:
                    System.out.println("===================== INSERT INFO ======================");
                    System.out.print("=>Enter Staff's ID : ");
                    while(!sc.hasNextInt()){
                        String input = sc.next();
                        System.out.print("=>Enter Staff's ID : ");
                    }
                    int Hid = sc.nextInt();
                    System.out.print("=>Enter Staff's Name : ");
                    sc.nextLine();
                    String Hname = sc.nextLine();
                    System.out.print("=>Enter Staff's Address : ");
                    String Haddress = sc.nextLine();
                    System.out.print("=>Enter Hour : ");
                    while(!sc.hasNextInt()){
                        String input = sc.next();
                        System.out.print("=>Enter Hour : ");
                    }
                    int Hhour = sc.nextInt();
                    System.out.print("=>Enter Rate : ");
                    while(!sc.hasNextDouble()){
                        String input = sc.next();
                        System.out.print("=>Enter Rate : ");
                    }
                    double Hrate = sc.nextDouble();
                    System.out.println("========================================================");
                    st = new HourlyEmployee(Hid, Hname, Haddress, Hhour, Hrate);
                    staff.add(st);
                    break;
                case 4:
                    Run = false;
                    break;
                default:
                    System.out.println("Enter 1 -> 4 ");
                    break;
            }
        }
    }
    public void Update(){
        int size = staff.size();
        System.out.println("=================== Choose ID UPDATE ===================");
        System.out.print("=>Enter Staff's ID : ");
        while(!sc.hasNextInt()){
            String input = sc.next();
            System.out.print("=>Enter Staff's ID : ");
        }
        int id = sc.nextInt();
        sc.nextLine();
        int index = ComparaID(id);
        System.out.println(index);
        if(index>=0) {
            System.out.println(staff.get(index));
            System.out.println("===================== UPDATE INFO ======================");
            if (staff.get(index) instanceof Volunteer) {
//            System.out.println("Object Volunteer");
                System.out.print("=>Enter Staff Member Name : ");
                String name = sc.nextLine();
                System.out.print("=>Enter Staff Address : ");
                String address = sc.nextLine();
                StaffMember st0 = new Volunteer(id, name, address);
                staff.set(index, st0);
            } else if (staff.get(index) instanceof SalariedEmployee) {
//            System.out.println("Object SalariedEmployee");
                System.out.print("=>Enter Staff Member Name : ");
                String Sname = sc.nextLine();
                System.out.print("=>Enter Staff Address : ");
                String Saddress = sc.nextLine();
                System.out.print("=>Enter Salary : ");
                while(!sc.hasNextDouble()){
                    String input = sc.next();
                    System.out.print("=>Enter Salary : ");
                }
                double salary = sc.nextDouble();
                System.out.print("=>Enter Bouns : ");
                while(!sc.hasNextDouble()){
                    String input = sc.next();
                    System.out.print("=>Enter Bouns : ");
                }
                double bonus = sc.nextDouble();
                StaffMember st1 = new SalariedEmployee(id, Sname, Saddress, salary, bonus);
                staff.set(index, st1);
            } else if (staff.get(index) instanceof HourlyEmployee) {
//            System.out.println("Object HourlyEmployee");
                System.out.print("=>Enter Staff Member Name : ");
                String Hname = sc.nextLine();
                System.out.print("=>Enter Staff Address : ");
                String Haddress = sc.nextLine();
                System.out.print("=>Enter Hour : ");
                while(!sc.hasNextInt()){
                    String input = sc.next();
                    System.out.print("=>Enter Hour : ");
                }
                int hour = sc.nextInt();
                System.out.print("=>Enter Rate : ");
                while(!sc.hasNextDouble()){
                    String input = sc.next();
                    System.out.print("=>Enter Rate : ");
                }
                double rate = sc.nextDouble();
                StaffMember st2 = new HourlyEmployee(id, Hname, Haddress, hour, rate);
                staff.set(index, st2);
            } else {
                System.out.println("Not Object");
            }
        }else{
            System.out.println("=>Sorry ID"+id+" not has!!!");
        }
//        System.out.println("Index Of = "+staff.get(id-1));
//        System.out.println("Index Of = "+staff.indexOf(id-1));
//        System.out.println("Index Of = "+staff.indexOf(staff.get(id-1)));// use real homework.
//        System.out.println("Index Of = "+staff.get(staff.indexOf(staff.get(id-1))));
//        System.out.println("Size "+staff.size());
//        System.out.print("Enter Name : ");
//        String name = sc.nextLine();
//        Volunteer v= new Volunteer(12,"String","pp");
//          SalariedEmployee v = new SalariedEmployee(4,"gogogo","pp",200,34);
//          staff.set(id-1,v);
//        staff.get(id-1).setName(name);

    }
    public int ComparaID(int id){
        int index = 0;
        for (int i=0;i<staff.size();i++) {
            if (((StaffMember) staff.get(i)).getId() == id) {
                index = i;
                break;
            }else{
                index =-1;
            }
        }
        return index;
    }
    public void Type(StaffMember sts){
        if(sts instanceof SalariedEmployee){
            System.out.println("Object SalariedEmployee");
        }else if(sts instanceof HourlyEmployee){
            System.out.println("HourlyEmployee");
        }else if(sts instanceof Volunteer){
            System.out.println("Volunteer");
        }else{
            System.out.println("not all");
        }
    }
    public void Remove(){
        System.out.println("===================== REMOVE INFO ======================");
        System.out.print("=>Enter Staff's ID : ");
        while(!sc.hasNextInt()){
            String input = sc.next();
            System.out.print("=>Enter Staff's ID : ");
        }
        int id = sc.nextInt();
//        System.out.println(ComparaID(id));
        int index = ComparaID(id);
        if(index>=0)
            staff.remove(index);
        else
            System.out.println("=>Sorry ID"+id+" not has!!!");
    }
}
